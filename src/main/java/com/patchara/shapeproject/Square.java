/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shapeproject;

/**
 *
 * @author user1
 */
public class Square {
    private double s;
    
    public Square(double s){
           this.s=s;
    }
    public double calAreaSq(){
        return s*s;
    }
    public double getS(){
        return s;
    }
    public void setS(double s){
        if(s<=0){
            System.out.println("Error : Side must more than zero");
            return;
        }
        this.s=s;
    }
    public String StrSq(){
        return "Side = "+this.getS()+" ,Then area of square1 is "+this.calAreaSq();
    }
}
