/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shapeproject;

/**
 *
 * @author user1
 */
public class Triangle {
    private double b,h;
    public static final double constant = 0.5;
    
    public Triangle(double b,double h){
           this.b=b;
           this.h=h;
    }
    public double calAreaTri(){
        return constant*b*h;
    }
    public double getB(){
        return b;
    }
    public double getH(){
        return h;
    }
    public void setBnsetH(double b,double h){
        if(h<=0||b<=0){
            System.out.println("Error : Base and Height must more than zero");
            return;
        }
        this.b=b;
        this.h=h;
    }
    public String StrTri(){
        return "Base = "+this.getB()+" and Height = "+this.getH()+" Then area of Triangle1 is "+this.calAreaTri();
    }
}
